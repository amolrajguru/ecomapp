$(document)
		.ready(
				function() {
					$("#address0country")
							.change(
									function() {
										var countryId = $(this).val();
										var s = '<option value=' + -1
												+ '>-SELECT-</option>';
										if (countryId > 0) {
											$
													.ajax({
														url : 'states',
														data : {
															"countryId" : countryId
														},
														success : function(
																result) {
															var result = JSON
																	.parse(result);
															for (var i = 0; i < result.length; i++) {
																s += '<option value="'
																		+ result[i][0]
																		+ '">'
																		+ result[i][1]
																		+ '</option>';
															}
															$('#address0state')
																	.html(s);
														}
													});
										}
										// reset data
										$('#address0state').html(s);
										$('#address0city').html(s);
									});

					$("#address0state")
							.change(
									function() {
										var stateId = $(this).val();
										var s = '<option value=' + -1
												+ '>-SELECT-</option>';
										if (stateId > 0) {
											$
													.ajax({
														url : 'cities',
														data : {
															"stateId" : stateId
														},
														success : function(
																result) {
															var result = JSON
																	.parse(result);
															for (var i = 0; i < result.length; i++) {
																s += '<option value="'
																		+ result[i][0]
																		+ '">'
																		+ result[i][1]
																		+ '</option>';
															}
															$('#address0city')
																	.html(s);
														}
													});
										}
										// reset data
										$('#address0city').html(s);
									});

					$("#sameAddr")
							.click(
									function() {
										var len = $("input[type='checkbox'][name='sameAddr']:checked").length;
										if (len == 1) {
											// read from address0 and set to
											// address1
											$("#address1Line1").val(
													$("#address0Line1").val());
											$("#address1Line2").val(
													$("#address0Line2").val());
											
											$("#address1country").val($("#address0country").val());
											$("#address1state").val($("#address0state").val());
											$("#address1city").val($("#address0city").val());
											$("#address1pincode")
													.val(
															$(
																	"#address0pincode")
																	.val());
										} else {
											// set address1 as a empty string
											$("#address1Line1").val('');
											$("#address1Line2").val('');
											$("#address1country").val('');
											$("#address1state").val('');
											$("#address1city").val('');
											$("#address1pincode").val('');
										}
									});

				});