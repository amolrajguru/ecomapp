$(document).ready(function() {
	// 1. hide error section
	$("#displayNameError").hide();
	$("#emailError").hide();
	$("#contactError").hide();

	// 2.Define error variable
	var displayNameError = false;
	var emailError = false;
	var contactError = false;

	// 3. validate function
	function validate_displayName() {
		var val = $("#displayName").val();
		var exp = /^[A-Za-z0-9\s\.]{2,60}$/;
		if (val == '') {
			$("#displayNameError").show();
			$("#displayNameError").html("<b>Name</b> can not be empty");
			$("#displayNameError").css('color', 'red');
			displayNameError = false;
		} else if (!exp.test(val)) {
			$("#displayNameError").show();
			$("#displayNameError").html("<b>Name</b> must be 2-60 chars only");
			$("#displayNameError").css('color', 'red');
			displayNameError = false;
		} else {
			$("#displayNameError").hide();
			displayNameError = true;
		}
		return displayNameError;
	}

	function validate_email() {
		var val = $("#email").val();
		var exp = /^[A-Za-z0-9\.\-\_]+\@[a-z]{4,30}\.[a-z\.]{3,6}$/;
		if (val == '') {
			$("#emailError").show();
			$("#emailError").html("<b>Email</b> can not empty");
			$("#emailError").css('color', 'red');
			emailError = false;
		} else if (!exp.test(val)) {
			$("#emailError").show();
			$("#emailError").html("<b>Email</b> id is invalid");
			$("#emailError").css('color', 'red');
			emailError = false;
		} else {
			var id = 0; // for register check
			if ($("#id").val() != undefined) { // for edit check
				emailError = true;
				id = $("#id").val();
			}
			$.ajax({
				url : 'validateMail',
				data : {
					"email" : val,
					"id" : id
				},
				success : function(resTxt) {
					if (resTxt != '') {
						$("#emailError").show();
						$("#emailError").html(resTxt);
						$("#emailError").css('color', 'red');
						emailError = false;
					} else {
						$("#emailError").hide();
						emailError = true;
					}
				}
			});
		}
		return emailError;
	}

	function validate_contact() {
		var val = $("#contact").val();
		var exp = /^[0-9]{10}$/;

		if (val == '') {
			$("#contactError").show();
			$("#contactError").html("<b>Contact</b> can not empty");
			$("#contactError").css('color', 'red');
			contactError = false;
		} else if (!exp.test(val)) {
			$("#contactError").show();
			$("#contactError").html("<b>Contact</b> id is invalid");
			$("#contactError").css('color', 'red');
			contactError = false;
		} else {
			var id = 0; // for register check
			if ($("#id").val() != undefined) { // for edit check
				contactError = true;
				id = $("#id").val();
			}
			$.ajax({
				url : 'validateContact',
				data : {
					"contact" : val,
					"id" : id
				},
				success : function(resTxt) {
					if (resTxt != '') {
						$("#contactError").show();
						$("#contactError").html(resTxt);
						$("#contactError").css('color', 'red');
						contactError = false;
					} else {
						$("#contactError").hide();
						contactError = true;
					}
				}
			});
		}
		return contactError;
	}

	// 4. Action event
	$("#displayName").keyup(function() {
		validate_displayName();
	});
	$("#email").keyup(function() {
		validate_email();
	});
	$("#contact").keyup(function() {
		validate_contact();
	});

	// 5. On submit
	$("#myUserForm").submit(function() {
		validate_displayName();
		validate_email();
		validate_contact();
		if (displayNameError && emailError && contactError)
			return true;
		else
			return false;
	});
});