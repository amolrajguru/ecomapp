/**
 * 
 */
package in.sahyandritech.ecom.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.sahyandritech.ecom.entity.CategoryType;

/**
 * @author Admin
 *
 */
public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {
	
	@Query("SELECT id, name FROM CategoryType")
	List<Object[]> getCategoryTypeIdAndName();

}
