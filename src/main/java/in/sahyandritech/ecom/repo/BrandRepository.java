/**
 * 
 */
package in.sahyandritech.ecom.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.sahyandritech.ecom.entity.Brand;

/**
 * @author Admin
 *
 */
public interface BrandRepository extends JpaRepository<Brand, Long> {
	
	@Query("SELECT id, name FROM Brand")
	List<Object[]> getBrandIdAndName();
	
	@Query("SELECT id, image FROM Brand")
	List<Object[]> getBrandIdAndImage();

}
