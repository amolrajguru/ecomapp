/**
 * 
 */
package in.sahyandritech.ecom.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.sahyandritech.ecom.entity.Coupon;

/**
 * @author Admin
 *
 */
public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
