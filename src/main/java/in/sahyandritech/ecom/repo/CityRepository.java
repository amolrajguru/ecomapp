/**
 * 
 */
package in.sahyandritech.ecom.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.sahyandritech.ecom.entity.City;

/**
 * @author Admin
 *
 */
public interface CityRepository extends JpaRepository<City, Integer>{
	
	@Query("SELECT ct.id, ct.name FROM City ct")
	List<Object[]> getAllCitys();


}
