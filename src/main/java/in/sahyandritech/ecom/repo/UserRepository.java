/**
 * 
 */
package in.sahyandritech.ecom.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.sahyandritech.ecom.entity.User;

/**
 * @author Admin
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByEmail(String email);

	Optional<User> findByContact(String contact);

	@Query("SELECT COUNT(email) FROM User WHERE email=:email")
	Integer getEmailCount(String email);

	@Query("SELECT COUNT(contact) FROM User WHERE contact=:contact")
	Integer getContactCount(String contact);

	@Query("SELECT COUNT(email) FROM User WHERE email=:email AND id!=:id")
	Integer getUserEmailCountForEdit(String email, Long id);

	@Query("SELECT COUNT(contact) FROM User WHERE contact=:contact AND id!=:id")
	Integer getUserContactCountForEdit(String contact, Long id);
	
	@Query("UPDATE User SET password=:encPwd WHERE id=:userId")
	void updateUserPwd(String encPwd, Long userId);

}
