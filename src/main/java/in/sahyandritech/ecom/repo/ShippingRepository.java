/**
 * 
 */
package in.sahyandritech.ecom.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.sahyandritech.ecom.entity.Shipping;

/**
 * @author Admin
 *
 */
public interface ShippingRepository extends JpaRepository<Shipping, Long> {

}
