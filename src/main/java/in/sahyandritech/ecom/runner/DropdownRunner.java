/**
 * 
 */
package in.sahyandritech.ecom.runner;

import java.util.Arrays;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import in.sahyandritech.ecom.entity.City;
import in.sahyandritech.ecom.entity.Country;
import in.sahyandritech.ecom.entity.State;
import in.sahyandritech.ecom.repo.CityRepository;
import in.sahyandritech.ecom.repo.CountryRepository;
import in.sahyandritech.ecom.repo.StateRepository;

/**
 * @author Admin
 *
 */
@Component
public class DropdownRunner implements CommandLineRunner {

	@Autowired
	private CityRepository cityRepo;

	@Autowired
	private StateRepository stateRepo;

	@Autowired
	private CountryRepository countryRepo;

	@Override
	public void run(String... args) throws Exception {
		City c1 = new City(1, "Mumbai");
		City c2 = new City(2, "Pune");
		City c3 = new City(3, "Thane");
		City c4 = new City(4, "Nashik");
		City c5 = new City(5, "Kolhapur");
		City c6 = new City(6, "Solapur");
		City c7 = new City(7, "Sangli");
		City c8 = new City(8, "Satara");
		City c9 = new City(9, "Raigad");
		City c10 = new City(10, "Ratnagiri");
		City c11 = new City(11, "Sindhudurg");
		City c12 = new City(12, "Ahmadnagar");
		City c13 = new City(13, "Aurangabad");
		City c14 = new City(14, "Jalna");
		City c15 = new City(15, "Parbhani");
		City c16 = new City(16, "Beed");
		City c17 = new City(17, "Osmanabad");
		City c18 = new City(18, "Nanded");
		City c19 = new City(19, "Latur");
		City c20 = new City(20, "Hingoli");
		City c21 = new City(21, "Buldhana");
		City c22 = new City(22, "Akola");
		City c23 = new City(23, "Washim");
		City c24 = new City(24, "Amaravati");
		City c25 = new City(25, "Yawatmal");
		City c26 = new City(26, "Wardha");
		City c27 = new City(27, "Nagpur");
		City c28 = new City(28, "Chandrapur");
		City c29 = new City(29, "Bhandara");
		City c30 = new City(30, "Gadchiroli");
		City c31 = new City(31, "Gondia");
		City c32 = new City(32, "Jalgaon");
		City c33 = new City(33, "Dhule");
		City c34 = new City(34, "Nandurbar");
		City c35 = new City(35, "Palghar");
		
		City c36 = new City(36, "Hydrabad");
		City c37 = new City(37,"Vishakhapattnam");
		City c38 = new City(38, "Sikandarabad");

		cityRepo.saveAll(Arrays.asList(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18,
				c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, c33, c34, c35,c36,c37,c38));

		State s1 = new State(101, "Maharashtra", Set.of(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15,
				c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, c33, c34, c35));
		
		State s2 = new State(102, "Telegana", Set.of(c36,c37,c38));
		stateRepo.saveAll(Arrays.asList(s1,s2));
		
		Country cn1 = new Country(201, "India", Set.of(s1));
		
		countryRepo.saveAll(Arrays.asList(cn1));
	}

}
