/**
 * 
 */
package in.sahyandritech.ecom.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import in.sahyandritech.ecom.constants.UserRole;
import in.sahyandritech.ecom.entity.User;
import in.sahyandritech.ecom.repo.UserRepository;
import in.sahyandritech.ecom.util.AppUtil;
import in.sahyandritech.ecom.util.MyMailUtil;

/**
 * @author Admin
 *
 */
@Component
public class AdminSetUpRunner implements CommandLineRunner {

	@Value("${app.admin.email}")
	private String username;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private MyMailUtil mailUtil;

	@Override
	public void run(String... args) throws Exception {

		if (!userRepo.findByEmail(username).isPresent()) {

			String pwd = AppUtil.genPwd();
			User user = new User();
			user.setDisplayName("ADMIN");
			user.setEmail(username);
			user.setPassword(encoder.encode(pwd));
			user.setRole(UserRole.ADMIN);
			user.setStatus("ACTIVE");
			user.setAddress("NONE");
			user.setContact("9763050252");
			Long id = userRepo.save(user).getId();

			if (id != null) { // only on user register (send email as a thread)
				new Thread(new Runnable() {
					public void run() {
						String text = "User is created! username : " + user.getEmail() + ", password:" + pwd
								+ ", with role: " + user.getRole().name();
						mailUtil.send(user.getEmail(), "User Register", text);
					}
				}).start();
			}
		}
	}

}
