/**
 * 
 */
package in.sahyandritech.ecom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Admin
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "city_tab")
public class City {

	@Id
	@Column(name = "city_id_col")
	private Integer id;

	@Column(name = "city_name_col")
	private String name;
}
