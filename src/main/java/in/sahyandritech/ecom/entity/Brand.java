/**
 * 
 */
package in.sahyandritech.ecom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Admin
 *
 */
@Entity
@Table(name = "brand_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brand {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name = "brand_name_col")
	private String name;
	@Column(name = "brand_code_col")
	private String code;
	@Column(name = "brand_tagLine_col")
	private String tagLine;
	@Column(name = "brand_image_col")
	private String image;
	@Column(name = "brand_note_col")
	private String note;

}
