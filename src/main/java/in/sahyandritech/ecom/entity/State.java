/**
 * 
 */
package in.sahyandritech.ecom.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Admin
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "state_tab")
public class State {

	@Id
	@Column(name = "state_id_col")
	private Integer id;

	@Column(name = "state_name_col")
	private String name;

	@OneToMany
	@JoinColumn(name = "state_id_fk")
	private Set<City> cities;
}
