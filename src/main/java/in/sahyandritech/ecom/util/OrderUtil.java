/**
 * 
 */
package in.sahyandritech.ecom.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import in.sahyandritech.ecom.constants.OrderStatus;
import in.sahyandritech.ecom.entity.CartItem;
import in.sahyandritech.ecom.entity.Order;
import in.sahyandritech.ecom.entity.OrderItem;

/**
 * @author Admin
 *
 */
@Component
public class OrderUtil {

	public Order mapCartItemsToOrder(List<CartItem> cartItems) {
		Order order = new Order();
		order.setStatus(OrderStatus.OPEN.name());
		List<OrderItem> orderItemsList = new ArrayList<>();
		for (CartItem item : cartItems) {
			OrderItem orderItem = new OrderItem();
			orderItem.setProduct(item.getProduct());
			orderItem.setQty(item.getQty());
			orderItem.setLineAmout(orderItem.getQty() * item.getProduct().getCost());
			orderItemsList.add(orderItem);
		}
		order.setOrderItem(orderItemsList);
		return order;
	}

	public void calculateGrandTotal(List<Order> orders) {
		for (Order order : orders) {
			Double amount = order.getOrderItem().stream().mapToDouble(ob -> ob.getLineAmout()).sum();
			order.setGrandTotal(amount);
		}
	}

}
