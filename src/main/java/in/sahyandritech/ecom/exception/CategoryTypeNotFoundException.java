/**
 * 
 */
package in.sahyandritech.ecom.exception;

/**
 * @author Admin
 *
 */
public class CategoryTypeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4510632496419070318L;

	public CategoryTypeNotFoundException() {
	}

	public CategoryTypeNotFoundException(String message) {
		super(message);
	}

}
