/**
 * 
 */
package in.sahyandritech.ecom.exception;

/**
 * @author Admin
 *
 */
public class BrandNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 328309395009167892L;

	public BrandNotFoundException() {
	}

	public BrandNotFoundException(String message) {
		super(message);
	}

}
