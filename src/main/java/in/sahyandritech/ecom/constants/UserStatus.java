/**
 * 
 */
package in.sahyandritech.ecom.constants;

/**
 * @author Admin
 *
 */
public enum UserStatus {

	ACTIVE, INACTIVE

}
