/**
 * 
 */
package in.sahyandritech.ecom.constants;

/**
 * @author Admin
 *
 */
public enum OrderStatus {

	OPEN, PLACED, ORDERED, SHIPPING,
	OUTFORDELIVERY, DELIVERED, 
	CANCELLED, RETURNED

}
