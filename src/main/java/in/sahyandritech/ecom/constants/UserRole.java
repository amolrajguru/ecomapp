/**
 * 
 */
package in.sahyandritech.ecom.constants;

/**
 * @author Admin
 *
 */
public enum UserRole {

	ADMIN, SALES, EMPLOYEE, CUSTOMER

}
