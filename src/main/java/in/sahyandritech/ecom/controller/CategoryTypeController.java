/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.CategoryType;
import in.sahyandritech.ecom.exception.CategoryTypeNotFoundException;
import in.sahyandritech.ecom.service.ICategoryTypeService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/categorytype")
public class CategoryTypeController {

	@Autowired
	private ICategoryTypeService categorytypeService;

	@GetMapping("/register")
	private String registerCategoryType(Model model) {
		model.addAttribute("categorytype", new CategoryType());
		return "CategoryTypeRegister";
	}

	@PostMapping("/save")
	public String saveCategoryType(@ModelAttribute CategoryType categorytype, Model model) {
		Long id = categorytypeService.saveCategoryType(categorytype);
		model.addAttribute("message", "CategoryType created with Id:" + id);
		model.addAttribute("categorytype", new CategoryType());
		return "CategoryTypeRegister";
	}

	@GetMapping("/all")
	public String getAllCategoryTypes(Model model, @RequestParam(value = "message", required = false) String message) {
		List<CategoryType> list = categorytypeService.getAllCategoryTypes();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "CategoryTypeData";
	}

	@GetMapping("/delete")
	public String deleteCategoryType(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			categorytypeService.deleteCategoryType(id);
			attributes.addAttribute("message", "CategoryType deleted with Id:" + id);
		} catch (CategoryTypeNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCategoryType(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			CategoryType ob = categorytypeService.getOneCategoryType(id);
			model.addAttribute("categorytype", ob);
			page = "CategoryTypeEdit";
		} catch (CategoryTypeNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCategoryType(@ModelAttribute CategoryType categorytype, RedirectAttributes attributes) {
		categorytypeService.updateCategoryType(categorytype);
		attributes.addAttribute("message", "CategoryType updated");
		return "redirect:all";
	}
}
