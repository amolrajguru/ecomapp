/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.exception.CustomerNotFoundException;
import in.sahyandritech.ecom.service.ICityService;
import in.sahyandritech.ecom.service.ICountryService;
import in.sahyandritech.ecom.service.ICustomerService;
import in.sahyandritech.ecom.service.IStateService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private ICountryService countryService;

	@Autowired
	private IStateService stateService;

	@Autowired
	private ICityService cityService;

	@GetMapping("/register")
	public String showRegister(Model model) {
		model.addAttribute("countryList", countryService.getAllCountries());
		model.addAttribute("stateList", stateService.getAllStates());
		model.addAttribute("cityList", cityService.getAllCitys());
		return "CustomerRegister";
	}

	@PostMapping("/save")
	public String saveCustomer(@ModelAttribute Customer customer, Model model) {
		Long id = customerService.saveCustomer(customer);
		model.addAttribute("message", "Customer '" + id + "' is created");
		return "CustomerRegister";
	}

	@GetMapping("/all")
	public String showCustomer(Model model) {
		model.addAttribute("list", customerService.getAllCustomers());
		return "CustomerData";
	}

	@GetMapping("/delete")
	public String deleteCustomer(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			customerService.deleteCustomer(id);
			attributes.addAttribute("message", "Customer deleted with Id:" + id);
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@ResponseBody
	@GetMapping("/states")
	public String getStates(@RequestParam Integer countryId, Model model) {
		String json = null;
		List<Object[]> list = countryService.getStatesByCountry(countryId);
		try {
			json = new ObjectMapper().writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}

	@ResponseBody
	@GetMapping("/cities")
	public String getCities(@RequestParam Integer stateId) {
		String json = null;
		List<Object[]> list = stateService.getCitiesByState(stateId);
		try {
			json = new ObjectMapper().writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
}
