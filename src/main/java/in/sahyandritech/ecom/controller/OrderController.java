/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.sahyandritech.ecom.entity.CartItem;
import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.entity.Order;
import in.sahyandritech.ecom.service.ICartItemService;
import in.sahyandritech.ecom.service.ICustomerService;
import in.sahyandritech.ecom.service.IOrderService;
import in.sahyandritech.ecom.util.OrderUtil;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private IOrderService orderService;

	@Autowired
	private ICartItemService cartItemService;

	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private ICustomerService customerService;

	@GetMapping("/create")
	public String placeOrder(HttpSession session) {
		Customer cust = (Customer) session.getAttribute("customer");
		List<CartItem> cartItems = cartItemService.viewAllItems(cust.getId());
		Order order = orderUtil.mapCartItemsToOrder(cartItems);
		order.setCustomer(cust);
		orderService.placeOrder(order);
		cartItemService.deleteAllCartItems(cartItems);
		session.setAttribute("cartItemsCount", 0);
		return "redirect:customerOrders";
	}

	@GetMapping("/customerOrders")
	public String getCustomerOrders(HttpSession session, Model model) {
		Customer cust = (Customer) session.getAttribute("customer");
		List<Order> orders = orderService.getOrdersByCustomerId(cust.getId());
		orderUtil.calculateGrandTotal(orders);
		model.addAttribute("list", orders);
		model.addAttribute("custAddr", customerService.getCustomerAddress(cust.getId()).get(0));
		return "CustomerOrderPage";
	}

	@GetMapping("/all")
	public String getAllOrders(Model model) {
		List<Order> orders = orderService.fetchAllOrders();
		model.addAttribute("list", orders);
		return "OrdersDataPage";
	}

	@GetMapping("/updateStatus")
	public String updateOrderStatus(@RequestParam Long id, @RequestParam String status) {
		orderService.updateOrderStatus(id, status);
		return "redirect:all";
	}
}
