/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.Product;
import in.sahyandritech.ecom.exception.ProductNotFoundException;
import in.sahyandritech.ecom.service.IBrandService;
import in.sahyandritech.ecom.service.ICategoryService;
import in.sahyandritech.ecom.service.IProductService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@Autowired
	private IBrandService brandService;

	@Autowired
	private ICategoryService categoryService;

	private void commonUi(Model model) {
		model.addAttribute("categories", categoryService.getCategoryIdAndName("ACTIVE"));
		model.addAttribute("brands", brandService.getBrandIdAndName());
	}

	// 1. Show register page
	@GetMapping("/register")
	public String saveProduct(Model model) {
		commonUi(model);
		return "ProductRegister";
	}

	// 2.Save Product
	@PostMapping("/save")
	public String saveProduct(@ModelAttribute Product product, Model model) {
		long id = productService.saveProduct(product);
		String message = "Product '" + id + "' is created";
		model.addAttribute("message", message);
		commonUi(model);
		return "ProductRegister";
	}

	// 3.List All Product
	@GetMapping("/all")
	public String showAll(Model model, @RequestParam(value = "message", required = false) String message) {
		List<Product> list = productService.getAllProducts();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "ProductData";

	}

	// 4.delete product
	@GetMapping("/delete")
	public String deleteProduct(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			productService.deleteProduct(id);
			attributes.addAttribute("message", "Product deleted with Id:" + id);
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editProduct(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Product ob = productService.getOneProduct(id);
			model.addAttribute("product", ob);
			commonUi(model);
			page = "ProductEdit";
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateProduct(@ModelAttribute Product product, RedirectAttributes attributes) {
		productService.updateProduct(product);
		attributes.addAttribute("message", "Product Updated");
		return "redirect:all";
	}

}
