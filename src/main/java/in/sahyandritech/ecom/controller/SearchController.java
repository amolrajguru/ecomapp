/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.sahyandritech.ecom.entity.Category;
import in.sahyandritech.ecom.entity.CategoryType;
import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.entity.Product;
import in.sahyandritech.ecom.service.IBrandService;
import in.sahyandritech.ecom.service.ICategoryService;
import in.sahyandritech.ecom.service.ICategoryTypeService;
import in.sahyandritech.ecom.service.IProductRatingService;
import in.sahyandritech.ecom.service.IProductService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping({ "/search", "/" })
public class SearchController {

	@Autowired
	private IBrandService brandService;

	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private ICategoryTypeService categoryTypeService;

	@Autowired
	private IProductRatingService productRatingService;

	@GetMapping("/showBrands")
	public String showBrand(Model model) {
		// get brand image and id
		List<Object[]> list = brandService.getBrandIdAndImage();
		model.addAttribute("list", list);
		return "BrandPage";
	}

	@GetMapping("/products")
	public String showProductByBrands(@RequestParam("brandId") Long brandId, Model model) {
		List<Object[]> list = productService.getProductByBrands(brandId);
		model.addAttribute("list", list);
		return "ProductsResult";
	}

	// show all category type
	@GetMapping("/categories")
	public String showAllCategoryByType(Model model) {
		List<CategoryType> list = categoryTypeService.getAllCategoryTypes();
		model.addAttribute("list", list);
		return "CategoryTypePage";
	}

	// show all category by categoryType
	@GetMapping("/getCategories")
	public String showAllCategoriesByType(@RequestParam Long catTypeId, Model model) {
		List<Category> list = categoryService.getCategoryByCategoryType(catTypeId);
		model.addAttribute("list", list);
		return "CategoriesPage";
	}

	// Show All Product By Category
	@GetMapping("/categoryProducts")
	public String showAllProductsByCategory(@RequestParam Long catId, Model model) {
		List<Object[]> list = productService.getProductByCategory(catId);
		model.addAttribute("list", list);
		return "ProductsResult";
	}

	@GetMapping("/byProduct")
	public String showAllProductsByName(@RequestParam String productname, Model model) {
		List<Object[]> list = productService.getProductByNameMatching(productname);
		model.addAttribute("list", list);
		return "ProductsResult";
	}

	@GetMapping("/productViewById")
	public String viewOneProductById(@RequestParam Long prodId, @RequestParam(required = false) String message,
			HttpSession session, Model model) {
		Product product = productService.getOneProduct(prodId);
		model.addAttribute("product", product);
		model.addAttribute("message", message);
		Customer cust = (Customer) session.getAttribute("customer");
		model.addAttribute("isRated", productRatingService.isCustomerRatedProduct(cust.getId(), prodId));
		model.addAttribute("ratingList", productRatingService.getAllProductRatings(prodId));
		return "ProductViewPage";
	}
}
