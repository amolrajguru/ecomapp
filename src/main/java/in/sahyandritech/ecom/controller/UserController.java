/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.constants.UserRole;
import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.entity.User;
import in.sahyandritech.ecom.exception.UserNotFoundException;
import in.sahyandritech.ecom.service.ICartItemService;
import in.sahyandritech.ecom.service.ICustomerService;
import in.sahyandritech.ecom.service.IUserService;
import in.sahyandritech.ecom.util.AppUtil;
import in.sahyandritech.ecom.util.MyMailUtil;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService userService;

	@Autowired
	private MyMailUtil mailUtil;

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private ICartItemService cartItemService;

	@GetMapping("/register")
	public String showReg() {
		return "UserRegister";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute User user, Model model) {
		Long id = userService.saveUser(user);
		model.addAttribute("message", "User created with id " + id);
		return "UserRegister";
	}

	@GetMapping("/all")
	public String showAllUser(Model model, @RequestParam(value = "message", required = false) String message) {
		List<User> list = userService.getAllUsers();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "UserData";
	}

	@GetMapping("/delete")
	public String deleteUser(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			userService.deleteUser(id);
			attributes.addAttribute("message", "User deleted with Id:" + id);
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			attributes.addFlashAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editUser(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			User ob = userService.getOneUser(id);
			model.addAttribute("user", ob);
			page = "UserEdit";
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			attributes.addFlashAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateUser(@ModelAttribute User user, RedirectAttributes attributes) {
		userService.updateUser(user);
		attributes.addAttribute("message", "User Updated");
		return "redirect:all";
	}

	@ResponseBody
	@GetMapping("/validateMail")
	public String validateEmail(@RequestParam("email") String email, @RequestParam Long id) {
		String message = "";
		if (id == 0 && userService.isEmailExist(email)) { // register check
			message = email + " already exist";
		} else if (id != 0 && userService.isUserEmailExistForEdit(email, id)) { // edit check
			message = email + " already exist";
		}
		return message;
	}

	@ResponseBody
	@GetMapping("/validateContact")
	public String validateContact(@RequestParam("contact") String contact, @RequestParam Long id) {
		String message = "";
		if (id == 0 && userService.isCountExist(contact)) { // register check
			message = contact + " already exist";
		} else if (id != 0 && userService.isUserContactExistForEdit(contact, id)) {
			message = contact + " already exist";
		}
		return message;
	}

	@GetMapping("/profile")
	public String showProfile() {
		return "UserProfile";
	}

	@GetMapping("/setup")
	public String setupUser(Authentication authentication, HttpSession session) {
		// fetch current user object
		User user = userService.findByEmail(authentication.getName()).get();
		String displayName = user.getDisplayName();
		session.setAttribute("displayName", displayName);

		// store in HttpSession
		session.setAttribute("userOb", user);
		String page = null;

		@SuppressWarnings("unchecked")
		String role = ((List<GrantedAuthority>) authentication.getAuthorities()).get(0).getAuthority();

		if (role.equals(UserRole.ADMIN.name())) {
			page = "/user/all";
		} else if (role.equals(UserRole.EMPLOYEE.name())) {
			page = "/brand/all";
		} else if (role.equals(UserRole.SALES.name())) {
			page = "/product/all";
		} else if (role.equals(UserRole.CUSTOMER.name())) {
			page = "/product/search";
			// setting customerId in HttpSession for one time
			Customer cust = customerService.findByEmail(authentication.getName());
			session.setAttribute("customer", cust);
			session.setAttribute("cartItemsCount", cartItemService.getCartItemCount(cust.getId()));
			page = "/search/showBrands";
		}
		return "redirect:" + page;
	}

	@GetMapping("/login")
	public String showLoginPage() {
		return "UserLogin";
	}

	@GetMapping("/showPwdUpdate")
	public String showPwdUpdate() {
		return "UserPwdUpdate";
	}

	@PostMapping("/pwdUpdate")
	public String updatePwd(@RequestParam String password, HttpSession session, Model model) {

		// read current user from session
		User user = (User) session.getAttribute("userOb");
		// read user id
		Long userId = user.getId();

		// make service call
		userService.updateUserPwd(password, userId);
		// TODO: EMAIL TASK
		model.addAttribute("message", "Password Updated!");
		return "UserPwdUpdate";
		// retrun "redirect:logout";
	}

	@GetMapping("/showForgot")
	public String showForgot() {
		return "UserNewPwdGen";
	}

	@PostMapping("/genNewPwd")
	public String genNewPwd(@RequestParam String email, Model model) {
		Optional<User> opt = userService.findByEmail(email);
		if (opt.isPresent()) {
			// read user object
			User user = opt.get();

			// Generate new Password
			String pwd = AppUtil.genPwd();

			// encode and update in database
			userService.updateUserPwd(pwd, user.getId());

			// send message to ui
			model.addAttribute("message", "Password Updated!Check your inbox!!");

			// send email to user
			if (user.getId() != null)
				new Thread(new Runnable() {
					public void run() {
						String text = "YOUR USERNAME IS : " + user.getEmail() + ", AND NEW PASSWORD IS " + pwd;
						mailUtil.send(user.getEmail(), "PASSWORD UPDATED", text);
					}
				}).start();
		} else { // if user not present
			model.addAttribute("message", "User Not Found!!");
		}
		return "UserNewPwdGen";

	}
}