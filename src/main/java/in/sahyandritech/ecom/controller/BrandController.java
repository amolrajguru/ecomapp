/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.Brand;
import in.sahyandritech.ecom.exception.BrandNotFoundException;
import in.sahyandritech.ecom.service.IBrandService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/brand")
public class BrandController {

	@Autowired
	private IBrandService brandService;

	@GetMapping("/register")
	public String registerBrand(Model model) {
		model.addAttribute("brand", new Brand());
		return "BrandRegister";
	}

	@PostMapping("/save")
	public String saveBrand(@ModelAttribute Brand brand, Model model) {
		Long id = brandService.saveBrand(brand);
		model.addAttribute("message", "Brand created with Id:" + id);
		model.addAttribute("brand", new Brand());
		return "BrandRegister";
	}

	@GetMapping("/all")
	public String getAllBrands(Model model, @RequestParam(value = "message", required = false) String message) {
		List<Brand> list = brandService.getAllBrands();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "BrandData";
	}

	@GetMapping("/delete")
	public String deleteBrand(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			brandService.deleteBrand(id);
			attributes.addAttribute("message", "Brand deleted with Id:" + id);
		} catch (BrandNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editBrand(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Brand ob = brandService.getOneBrand(id);
			model.addAttribute("brand", ob);
			page = "BrandEdit";
		} catch (BrandNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateBrand(@ModelAttribute Brand brand, RedirectAttributes attributes) {
		brandService.updateBrand(brand);
		attributes.addAttribute("message", "Brand Updated");
		return "redirect:all";
	}
}
