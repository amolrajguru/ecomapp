/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.Coupon;
import in.sahyandritech.ecom.exception.CouponNotFoundException;
import in.sahyandritech.ecom.service.ICouponService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/coupon")
public class CouponController {

	@Autowired
	private ICouponService couponService;

	@GetMapping("/register")
	private String registerCoupon(Model model) {
		model.addAttribute("coupon", new Coupon());
		return "CouponRegister";
	}

	@PostMapping("/save")
	public String saveCoupon(@ModelAttribute Coupon coupon, Model model) {
		Long id = couponService.saveCoupon(coupon);
		model.addAttribute("message", "Coupon created with id:" + id);
		model.addAttribute("coupon", new Coupon());
		return "CouponRegister";
	}

	@GetMapping("/all")
	public String getAllCoupons(Model model, @RequestParam(value = "message", required = false) String message) {
		List<Coupon> list = couponService.getAllCoupons();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "CouponData";
	}

	@GetMapping("/delete")
	public String deleteCoupon(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			couponService.deleteCoupon(id);
			attributes.addAttribute("message", "Coupon deleted with id: " + id);
		} catch (CouponNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCoupon(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Coupon ob = couponService.getOneCoupon(id);
			model.addAttribute("coupon", ob);
			page = "CouponEdit";
		} catch (CouponNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String update(@ModelAttribute Coupon coupon, RedirectAttributes attributes) {
		couponService.updateCoupon(coupon);
		attributes.addAttribute("messgage", "Coupon updated!");
		return "redirect:all";
	}
}
