/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.Category;
import in.sahyandritech.ecom.exception.CategoryNotFoundException;
import in.sahyandritech.ecom.service.ICategoryService;
import in.sahyandritech.ecom.service.ICategoryTypeService;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private ICategoryTypeService categorytypeService;

	private void commonUi(Model model) {
		model.addAttribute("categoryTypes", categorytypeService.getCategoryTypeIdAndName());
	}

	@GetMapping("/register")
	public String registerCategory(Model model) {
		model.addAttribute("category", new Category());
		commonUi(model);
		return "CategoryRegister";
	}

	@PostMapping("/save")
	public String saveCategory(@ModelAttribute Category category, Model model) {
		Long id = categoryService.saveCategory(category);
		model.addAttribute("message", "Category created with Id:" + id);
		model.addAttribute("category", new Category());
		commonUi(model);
		return "CategoryRegister";
	}

	@GetMapping("/all")
	public String getAllCategory(Model model, @RequestParam(value = "message", required = false) String message) {
		List<Category> list = categoryService.getAllCategorys();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "CategoryData";
	}

	@GetMapping("/delete")
	public String deleteCategory(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			categoryService.deleteCategory(id);
			attributes.addAttribute("message", "Category deleted with Id:" + id);
		} catch (CategoryNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCategory(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Category ob = categoryService.getOneCategory(id);
			model.addAttribute("category", ob);
			page = "CategoryEdit";
			commonUi(model);
		} catch (CategoryNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCategory(@ModelAttribute Category category, RedirectAttributes attributes) {
		categoryService.updateCategory(category);
		attributes.addAttribute("message", "Category updated");
		return "redirect:all";
	}
}
