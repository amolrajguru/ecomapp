/**
 * 
 */
package in.sahyandritech.ecom.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.sahyandritech.ecom.entity.CartItem;
import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.service.ICartItemService;
import in.sahyandritech.ecom.service.IProductService;
import in.sahyandritech.ecom.util.UserUtil;

/**
 * @author Admin
 *
 */
@Controller
@RequestMapping("/cart")
public class CartController {

	@Autowired
	private ICartItemService cartService;

	@Autowired
	private IProductService productService;

	@Autowired
	private UserUtil userUtil;

	@GetMapping("/add")
	public String addItemToCart(Principal p, HttpSession session, @RequestParam Long prodId,
			RedirectAttributes attributes) {
		if (userUtil.isUserLoggedIn(p)) {
			Customer cust = (Customer) session.getAttribute("customer");
			// first check is product added to cart already or not
			CartItem cartItem = cartService.getOneCartItem(cust.getId(), prodId);
			if (cartItem == null) {
				// add
				CartItem newCartItem = new CartItem();
				newCartItem.setCustomer(cust);
				newCartItem.setProduct(productService.getOneProduct(prodId));
				newCartItem.setQty(1);
				cartService.addCartItem(newCartItem);
				attributes.addAttribute("message", "Product Added to cart!");
				Integer count = (Integer) session.getAttribute("cartItemsCount");
				count = count + 1;
				session.setAttribute("cartItemsCount", count);
			} else {
				attributes.addAttribute("message", "Product Quantity increased!");
				cartService.updateQty(cartItem.getId(), 1);
			}
		} else {
			attributes.addAttribute("message", "User must login to user cart");
		}
		attributes.addAttribute("prodId", prodId);
		return "redirect:/search/productViewById";
	}

	@GetMapping("/all")
	public String showCartItem(Principal p, HttpSession session, Model model) {
		Customer cust = (Customer) session.getAttribute("customer");
		List<CartItem> list = cartService.viewAllItems(cust.getId());
		model.addAttribute("list", list);
		return "CartItemPage";
	}

	@GetMapping("/remove")
	public String removeCartItem(@RequestParam Long cartItemId, HttpSession session) {
		cartService.removeCartItem(cartItemId);
		Integer count = (Integer) session.getAttribute("cartItemsCount");
		count = count - 1;
		session.setAttribute("cartItemsCount", count);
		return "redirect:all";
	}

	@GetMapping("/increase")
	public String increaseCartItemQty(@RequestParam Long cartItemId) {
		cartService.updateQty(cartItemId, 1);
		return "redirect:all";
	}
	
	@GetMapping("/decrease")
	public String decreaseCartItemQty(@RequestParam Long cartItemId) {
		cartService.updateQty(cartItemId, -1);
		return "redirect:/cart/all";
		
	}
}
