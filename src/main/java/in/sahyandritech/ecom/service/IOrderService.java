/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.Order;

/**
 * @author Admin
 *
 */
public interface IOrderService {

	Long placeOrder(Order order);

	List<Order> getOrdersByCustomerId(Long id);

	List<Order> fetchAllOrders();

	void updateOrderStatus(Long id, String status);

	List<Order> findByOrderStatus(String status);

}
