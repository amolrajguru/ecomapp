/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;
import java.util.Map;

import in.sahyandritech.ecom.entity.Brand;

/**
 * @author Admin
 *
 */
public interface IBrandService {

	Long saveBrand(Brand brand);

	void updateBrand(Brand brand);

	void deleteBrand(Long id);

	Brand getOneBrand(Long id);

	List<Brand> getAllBrands();

	Map<Long, String> getBrandIdAndName();

	List<Object[]> getBrandIdAndImage();
}
