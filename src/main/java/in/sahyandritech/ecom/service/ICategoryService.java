/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;
import java.util.Map;

import in.sahyandritech.ecom.entity.Category;

/**
 * @author Admin
 *
 */
public interface ICategoryService {

	Long saveCategory(Category category);

	void updateCategory(Category category);

	void deleteCategory(Long id);

	Category getOneCategory(Long id);

	List<Category> getAllCategorys();

	Map<Long, String> getCategoryIdAndName(String status);

	List<Category> getCategoryByCategoryType(Long id);
}
