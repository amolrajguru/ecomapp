/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

/**
 * @author Admin
 *
 */
public interface ICountryService {

	List<Object[]> getAllCountries();

	List<Object[]> getStatesByCountry(Integer id);

}
