/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

/**
 * @author Admin
 *
 */
public interface IStateService {

	List<Object[]> getAllStates();

	List<Object[]> getCitiesByState(Integer id);

}
