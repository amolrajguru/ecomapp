/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.ProductRating;

/**
 * @author Admin
 *
 */
public interface IProductRatingService {

	Long createProductRating(ProductRating pr);

	boolean isCustomerRatedProduct(Long custId, Long prodId);

	List<ProductRating> getAllProductRatings(Long prodId);

}
