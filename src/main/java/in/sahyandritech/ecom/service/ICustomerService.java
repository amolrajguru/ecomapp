/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.Address;
import in.sahyandritech.ecom.entity.Customer;

/**
 * @author Admin
 *
 */
public interface ICustomerService {

	Long saveCustomer(Customer customer);

	void uodateCustomer(Customer customer);

	List<Customer> getAllCustomers();

	void deleteCustomer(Long id);

	Customer getOneCustomer(Long id);

	Customer findByEmail(String email);

	List<Address> getCustomerAddress(Long id);

}
