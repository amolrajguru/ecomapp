/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;
import java.util.Map;

import in.sahyandritech.ecom.entity.CategoryType;

/**
 * @author Admin
 *
 */
public interface ICategoryTypeService {

	Long saveCategoryType(CategoryType categorytype);

	void updateCategoryType(CategoryType categorytype);

	void deleteCategoryType(Long id);

	CategoryType getOneCategoryType(Long id);

	List<CategoryType> getAllCategoryTypes();
	
	Map<Integer, String> getCategoryTypeIdAndName();

}
