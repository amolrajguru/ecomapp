/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;
import java.util.Map;

import in.sahyandritech.ecom.entity.Product;

/**
 * @author Admin
 *
 */
public interface IProductService {

	Long saveProduct(Product p);

	void updateProduct(Product p);

	void deleteProduct(Long id);

	Product getOneProduct(Long id);

	List<Product> getAllProducts();

	Map<Long, String> getProductIdAndNames();

	List<Object[]> getProductByBrands(Long brandId);

	List<Object[]> getProductByCategory(Long catId);

	List<Object[]> getProductByNameMatching(String input);
}
