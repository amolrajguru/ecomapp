/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.CartItem;

/**
 * @author Admin
 *
 */
public interface ICartItemService {

	Long addCartItem(CartItem cartItem);

	void removeCartItem(Long cartItemId);

	List<CartItem> viewAllItems(Long custId);

	CartItem getOneCartItem(Long custId, Long prodId);

	void updateQty(Long cartItemId, Integer qty);

	int getCartItemCount(Long custId);

	void deleteAllCartItems(List<CartItem> cartItem);
}
