/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.Coupon;

/**
 * @author Admin
 *
 */
public interface ICouponService {

	Long saveCoupon(Coupon coupon);

	void updateCoupon(Coupon coupon);

	void deleteCoupon(Long id);

	Coupon getOneCoupon(Long id);

	List<Coupon> getAllCoupons();

}
