/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.Stock;

/**
 * @author Admin
 *
 */
public interface IStockService {

	Long createStock(Stock stock);

	void updateStock(Long id, Long count);

	Long getStockIdByProduct(Long productId);

	List<Stock> getStockDetails();

}
