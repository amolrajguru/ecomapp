/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.sahyandritech.ecom.entity.Coupon;
import in.sahyandritech.ecom.repo.CouponRepository;
import in.sahyandritech.ecom.service.ICouponService;

/**
 * @author Admin
 *
 */
@Service
public class CouponServiceImpl implements ICouponService {

	@Autowired
	private CouponRepository couponRepo;

	@Override
	@Transactional
	public Long saveCoupon(Coupon coupon) {
		return couponRepo.save(coupon).getId();
	}

	@Override
	@Transactional
	public void updateCoupon(Coupon coupon) {
		couponRepo.save(coupon);
	}

	@Override
	@Transactional
	public void deleteCoupon(Long id) {
		couponRepo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Coupon getOneCoupon(Long id) {
		return couponRepo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Coupon> getAllCoupons() {
		return couponRepo.findAll();
	}
}
