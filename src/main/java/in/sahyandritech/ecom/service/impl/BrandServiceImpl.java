/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.sahyandritech.ecom.entity.Brand;
import in.sahyandritech.ecom.repo.BrandRepository;
import in.sahyandritech.ecom.service.IBrandService;
import in.sahyandritech.ecom.util.AppUtil;

/**
 * @author Admin
 *
 */
@Service
public class BrandServiceImpl implements IBrandService {

	@Autowired
	private BrandRepository brandRepo;

	@Override
	@Transactional
	public Long saveBrand(Brand brand) {
		return brandRepo.save(brand).getId();
	}

	@Override
	@Transactional
	public void updateBrand(Brand brand) {
		brandRepo.save(brand);
	}

	@Override
	@Transactional
	public void deleteBrand(Long id) {
		brandRepo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Brand getOneBrand(Long id) {
		return brandRepo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Brand> getAllBrands() {
		return brandRepo.findAll();
	}

	@Override
	public Map<Long, String> getBrandIdAndName() {
		List<Object[]> list = brandRepo.getBrandIdAndName();
		return AppUtil.convertListToMapLong(list);
	}

	@Override
	public List<Object[]> getBrandIdAndImage() {
		return brandRepo.getBrandIdAndImage();
	}
}
