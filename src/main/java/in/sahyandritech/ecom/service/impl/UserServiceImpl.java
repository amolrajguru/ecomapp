/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.constants.UserStatus;
import in.sahyandritech.ecom.entity.User;
import in.sahyandritech.ecom.exception.UserNotFoundException;
import in.sahyandritech.ecom.repo.UserRepository;
import in.sahyandritech.ecom.service.IUserService;
import in.sahyandritech.ecom.util.AppUtil;
import in.sahyandritech.ecom.util.MyMailUtil;

/**
 * @author Admin
 *
 */
@Service
public class UserServiceImpl implements IUserService, UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private MyMailUtil mailUtil;

	@Override
	public Long saveUser(User user) {
		// generate password
		String pwd = AppUtil.genPwd();

		// read generated password and encode
		String encPwd = encoder.encode(pwd);
		// set back to user object
		user.setPassword(encPwd);
		user.setStatus(UserStatus.INACTIVE.name());
		Long id = userRepo.save(user).getId();

		if (id != null) { // only on user register(sent email as a thread)
			new Thread(new Runnable() {
				public void run() {
					String text = "User is created! username : " + user.getEmail() + ", password: " + pwd
							+ ", with role: " + user.getRole().name();
					mailUtil.send(user.getEmail(), "User Register", text);
				}
			}).start();
		}
		return id;
	}

	@Override
	public void deleteUser(Long id) {
		userRepo.delete(getOneUser(id));
	}

	@Override
	public User getOneUser(Long id) {
		return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException("User not found"));
	}

	@Override
	public void updateUser(User user) {
		if (user.getId() == null || !userRepo.existsById(user.getId()))
			throw new UserNotFoundException("User not found");
		else
			userRepo.save(user);
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return userRepo.findByEmail(email);
	}

	@Override
	public Optional<User> findByContact(String contact) {
		return userRepo.findByContact(contact);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public boolean isEmailExist(String email) {
		return userRepo.getEmailCount(email) > 0;
	}

	@Override
	public boolean isCountExist(String contact) {
		return userRepo.getContactCount(contact) > 0;
	}

	@Override
	public long getUserCount() {
		return userRepo.count();
	}

	@Override
	public boolean isUserEmailExistForEdit(String email, Long id) {
		return userRepo.getUserEmailCountForEdit(email, id) > 0;
	}

	@Override
	public boolean isUserContactExistForEdit(String contact, Long id) {
		return userRepo.getUserContactCountForEdit(contact, id) > 0;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// load user by username(email)
		Optional<User> opt = findByEmail(username);
		if (!opt.isPresent()) {
			throw new UsernameNotFoundException("Not Exist");
		} else {
			// read user object
			User user = opt.get();
			return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
					Arrays.asList(new SimpleGrantedAuthority(user.getRole().name())));
		}
	}

	@Override
	public void updateUserPwd(String pwd, Long userId) {
         String encPwd = encoder.encode(pwd);
         userRepo.updateUserPwd(encPwd, userId);
	}

}
