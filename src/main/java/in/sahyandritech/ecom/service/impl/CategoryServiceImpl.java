/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.sahyandritech.ecom.entity.Category;
import in.sahyandritech.ecom.repo.CategoryRepository;
import in.sahyandritech.ecom.service.ICategoryService;
import in.sahyandritech.ecom.util.AppUtil;

/**
 * @author Admin
 *
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepo;

	@Override
	@Transactional
	public Long saveCategory(Category category) {
		return categoryRepo.save(category).getId();
	}

	@Override
	@Transactional
	public void updateCategory(Category category) {
		categoryRepo.save(category);
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		categoryRepo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Category getOneCategory(Long id) {
		return categoryRepo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Category> getAllCategorys() {
		return categoryRepo.findAll();
	}

	@Override
	public Map<Long, String> getCategoryIdAndName(String status) {
		List<Object[]> list =categoryRepo.getCategoryIdAndName(status);
		return AppUtil.convertListToMapLong(list);
	}

	@Override
	public List<Category> getCategoryByCategoryType(Long id) {
		return categoryRepo.getCategoryByCategoryType(id);
	}
}
