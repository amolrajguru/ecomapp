/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.entity.Order;
import in.sahyandritech.ecom.repo.OrderRepository;
import in.sahyandritech.ecom.service.IOrderService;

/**
 * @author Admin
 *
 */
@Service
public class OrderServiceImpl implements IOrderService {

	@Autowired
	private OrderRepository orderRepo;

	@Override
	public Long placeOrder(Order order) {
		return orderRepo.save(order).getId();
	}

	@Override
	public List<Order> getOrdersByCustomerId(Long id) {
		return orderRepo.getOrdersByCustomer(id);
	}

	@Override
	public List<Order> fetchAllOrders() {
		return orderRepo.findAll();
	}

	@Override
	public void updateOrderStatus(Long id, String status) {
         orderRepo.updateOrderStatus(id, status);
	}

	@Override
	public List<Order> findByOrderStatus(String status) {
		return orderRepo.findByStatus(status);
	}

}
