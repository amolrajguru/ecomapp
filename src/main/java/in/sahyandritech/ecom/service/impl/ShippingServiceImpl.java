/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.entity.Shipping;
import in.sahyandritech.ecom.exception.ShippingNotFoundException;
import in.sahyandritech.ecom.repo.ShippingRepository;
import in.sahyandritech.ecom.service.IShippingService;

/**
 * @author Admin
 *
 */
@Service
public class ShippingServiceImpl implements IShippingService {

	@Autowired
	private ShippingRepository shippingRepo;

	@Override
	public Long saveShipping(Shipping shipping) {
		return shippingRepo.save(shipping).getId();
	}

	@Override
	public void updateShipping(Shipping shipping) {
		if (shipping.getId() == null || !shippingRepo.existsById(shipping.getId()))
			throw new ShippingNotFoundException("Shipping Not Exist");
		else
			shippingRepo.save(shipping);
	}

	@Override
	public void deleteShipping(Long id) {
		shippingRepo.delete(getOneShipping(id));
	}

	@Override
	public Shipping getOneShipping(Long id) {
		return shippingRepo.findById(id).orElseThrow(() -> new ShippingNotFoundException("Shipping not Exist"));
	}

	@Override
	public List<Shipping> getAllShippings() {
		return shippingRepo.findAll();
	}

}
