/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.entity.CartItem;
import in.sahyandritech.ecom.repo.CartItemRepository;
import in.sahyandritech.ecom.service.ICartItemService;

/**
 * @author Admin
 *
 */
@Service
public class CartItemServiceImpl implements ICartItemService {

	@Autowired
	private CartItemRepository cartItemRepo;

	@Override
	public Long addCartItem(CartItem cartItem) {
		return cartItemRepo.save(cartItem).getId();
	}

	@Override
	public void removeCartItem(Long cartItemId) {
		cartItemRepo.deleteById(cartItemId);
	}

	@Override
	public List<CartItem> viewAllItems(Long custId) {
		return cartItemRepo.fetchCartItemsByCustomer(custId);
	}

	@Override
	public CartItem getOneCartItem(Long custId, Long prodId) {
		Optional<CartItem> opt = cartItemRepo.fetchCartItemByCustomerAndProduct(custId, prodId);
		if (opt.isPresent())
			return opt.get();
		else
			return null;
	}

	@Override
	@Transactional
	public void updateQty(Long cartItemId, Integer qty) {
		cartItemRepo.updateCartItemQty(cartItemId, qty);
	}

	@Override
	public int getCartItemCount(Long custId) {
		return cartItemRepo.getCartItemCount(custId);
	}

	@Override
	public void deleteAllCartItems(List<CartItem> cartItem) {
		cartItemRepo.deleteAll(cartItem);
	}
}
