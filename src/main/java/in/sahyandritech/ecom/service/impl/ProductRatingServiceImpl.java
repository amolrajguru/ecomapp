/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.entity.ProductRating;
import in.sahyandritech.ecom.repo.ProductRatingRepository;
import in.sahyandritech.ecom.service.IProductRatingService;

/**
 * @author Admin
 *
 */
@Service
public class ProductRatingServiceImpl implements IProductRatingService {

	@Autowired
	private ProductRatingRepository productRatingRepo;

	@Override
	public Long createProductRating(ProductRating pr) {
		return productRatingRepo.save(pr).getId();
	}

	@Override
	public boolean isCustomerRatedProduct(Long custId, Long prodId) {
		return productRatingRepo.getCustomerRating(custId, prodId) > 0;
	}

	@Override
	public List<ProductRating> getAllProductRatings(Long prodId) {
		return productRatingRepo.getAllProductRatings(prodId);
	}

}
