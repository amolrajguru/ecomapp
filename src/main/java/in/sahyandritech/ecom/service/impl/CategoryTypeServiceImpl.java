/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.sahyandritech.ecom.entity.CategoryType;
import in.sahyandritech.ecom.repo.CategoryTypeRepository;
import in.sahyandritech.ecom.service.ICategoryTypeService;
import in.sahyandritech.ecom.util.AppUtil;

/**
 * @author Admin
 *
 */
@Service
public class CategoryTypeServiceImpl implements ICategoryTypeService {

	@Autowired
	private CategoryTypeRepository categorytypeRepo;

	@Override
	@Transactional
	public Long saveCategoryType(CategoryType categorytype) {
		return categorytypeRepo.save(categorytype).getId();
	}

	@Override
	@Transactional
	public void updateCategoryType(CategoryType categorytype) {
		categorytypeRepo.save(categorytype);
	}

	@Override
	@Transactional
	public void deleteCategoryType(Long id) {
		categorytypeRepo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public CategoryType getOneCategoryType(Long id) {
		return categorytypeRepo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CategoryType> getAllCategoryTypes() {
		return categorytypeRepo.findAll();
	}

	@Override
	public Map<Integer, String> getCategoryTypeIdAndName() {
		List<Object[]> list = categorytypeRepo.getCategoryTypeIdAndName();
		return AppUtil.convertListToMap(list);
	}

}
