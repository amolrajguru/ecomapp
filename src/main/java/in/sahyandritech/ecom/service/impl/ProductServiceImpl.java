/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.entity.Product;
import in.sahyandritech.ecom.exception.ProductNotFoundException;
import in.sahyandritech.ecom.repo.ProductRepository;
import in.sahyandritech.ecom.service.IProductService;
import in.sahyandritech.ecom.util.AppUtil;

/**
 * @author Admin
 *
 */
@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductRepository productRepo;

	@Override
	public Long saveProduct(Product p) {
		return productRepo.save(p).getId();
	}

	@Override
	public void updateProduct(Product p) {
		if (p.getId() == null || !productRepo.existsById(p.getId()))
			throw new ProductNotFoundException("Product Not Found");
		else
			productRepo.save(p);
	}

	@Override
	public void deleteProduct(Long id) {
		productRepo.delete(getOneProduct(id));
	}

	@Override
	public Product getOneProduct(Long id) {
		return productRepo.findById(id).orElseThrow(() -> new ProductNotFoundException("Product Not Found"));
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public Map<Long, String> getProductIdAndNames() {
		List<Object[]> list = productRepo.getProductIdAndNames();
		return AppUtil.convertListToMapLong(list);
	}
	
	@Override
	public List<Object[]> getProductByBrands(Long brandId) {
		return productRepo.getProductByBrands(brandId);
	}

	@Override
	public List<Object[]> getProductByCategory(Long catId) {
		return productRepo.getProductByCategory(catId);
	}
	
	@Override
	public List<Object[]> getProductByNameMatching(String input) {
		return productRepo.getProductByNameMatching(input);
	}
}
