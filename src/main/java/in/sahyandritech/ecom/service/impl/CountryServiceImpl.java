/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.repo.CountryRepository;
import in.sahyandritech.ecom.service.ICountryService;

/**
 * @author Admin
 *
 */
@Service
public class CountryServiceImpl implements ICountryService {

	@Autowired
	private CountryRepository countryRepo;

	@Override
	public List<Object[]> getAllCountries() {
		return countryRepo.getAllCountries();
	}

	@Override
	public List<Object[]> getStatesByCountry(Integer id) {
		return countryRepo.getStatesByCountry(id);
	}

}
