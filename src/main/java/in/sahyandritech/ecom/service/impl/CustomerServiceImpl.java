/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.constants.UserRole;
import in.sahyandritech.ecom.entity.Address;
import in.sahyandritech.ecom.entity.Customer;
import in.sahyandritech.ecom.entity.User;
import in.sahyandritech.ecom.exception.CustomerNotFoundException;
import in.sahyandritech.ecom.repo.CustomerRepository;
import in.sahyandritech.ecom.service.ICustomerService;
import in.sahyandritech.ecom.service.IUserService;

/**
 * @author Admin
 *
 */
@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private IUserService userService;

	@Override
	public Long saveCustomer(Customer customer) {
		Long id = customerRepo.save(customer).getId();
		if (id != null) {
			User user = new User();
			user.setDisplayName(customer.getName());
			user.setEmail(customer.getEmail());
			user.setContact(customer.getMobile());
			user.setRole(UserRole.CUSTOMER);
			user.setAddress(customer.getAddress().get(0).toString());
			userService.saveUser(user);
		}
		return id;
	}

	@Override
	public void uodateCustomer(Customer customer) {
		if (customer.getId() == null || !customerRepo.existsById(customer.getId()))
			throw new CustomerNotFoundException("Customer not found");
		else
			customerRepo.save(customer);
	}

	@Override
	public void deleteCustomer(Long id) {
		customerRepo.delete(getOneCustomer(id));
	}

	@Override
	public Customer getOneCustomer(Long id) {
		return customerRepo.findById(id).orElseThrow(() -> new CustomerNotFoundException("Customer Not Found"));

	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerRepo.findAll();
	}

	@Override
	public Customer findByEmail(String email) {
		return customerRepo.findByEmail(email).get();
	}
	
	@Override
	public List<Address> getCustomerAddress(Long id) {
		return customerRepo.getCustomerAddress(id);
	}

}
