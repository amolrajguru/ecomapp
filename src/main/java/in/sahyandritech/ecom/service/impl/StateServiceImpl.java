/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.repo.StateRepository;
import in.sahyandritech.ecom.service.IStateService;

/**
 * @author Admin
 *
 */
@Service
public class StateServiceImpl implements IStateService {

	@Autowired
	private StateRepository stateRepo;
	
	@Override
	public List<Object[]> getAllStates() {
		return stateRepo.getAllStates();
	}
	
	@Override
	public List<Object[]> getCitiesByState(Integer id) {
		return stateRepo.getCitiesByStates(id);
	}

}
