/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sahyandritech.ecom.repo.CityRepository;
import in.sahyandritech.ecom.service.ICityService;

/**
 * @author Admin
 *
 */
@Service
public class CityServiceImpl implements ICityService {

	@Autowired
	private CityRepository cityRepo;
	
	@Override
	public List<Object[]> getAllCitys() {
		return cityRepo.getAllCitys();
	}

}
