/**
 * 
 */
package in.sahyandritech.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.sahyandritech.ecom.entity.Stock;
import in.sahyandritech.ecom.repo.StockRepository;
import in.sahyandritech.ecom.service.IStockService;

/**
 * @author Admin
 *
 */
@Service
public class StockServiceImpl implements IStockService {
	
	@Autowired
	private StockRepository stockRepo;

	@Override
	public Long createStock(Stock stock) {
		return stockRepo.save(stock).getId();
	}

	@Override
	@Transactional
	public void updateStock(Long id, Long count) {
        stockRepo.updateStock(id, count);
	}

	@Override
	public Long getStockIdByProduct(Long productId) {
		return stockRepo.getStockIdByProduct(productId);
	}

	@Override
	public List<Stock> getStockDetails() {
		return stockRepo.findAll();
	}

}
