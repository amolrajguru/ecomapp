/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;
import java.util.Optional;

import in.sahyandritech.ecom.entity.User;

/**
 * @author Admin
 *
 */
public interface IUserService {

	Long saveUser(User user);

	void updateUser(User user);

	void deleteUser(Long id);

	User getOneUser(Long id);

	Optional<User> findByEmail(String email);

	Optional<User> findByContact(String contact);

	List<User> getAllUsers();

	public boolean isEmailExist(String email);

	public boolean isCountExist(String contact);

	public boolean isUserEmailExistForEdit(String email, Long id);

	public boolean isUserContactExistForEdit(String contact, Long id);
	
	long getUserCount();

	void updateUserPwd(String pwd, Long userId);
}
