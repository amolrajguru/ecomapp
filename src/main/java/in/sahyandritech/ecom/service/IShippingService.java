/**
 * 
 */
package in.sahyandritech.ecom.service;

import java.util.List;

import in.sahyandritech.ecom.entity.Shipping;

/**
 * @author Admin
 *
 */
public interface IShippingService {

	Long saveShipping(Shipping shipping);

	void updateShipping(Shipping shipping);

	void deleteShipping(Long id);

	Shipping getOneShipping(Long id);

	List<Shipping> getAllShippings();

}
